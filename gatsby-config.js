module.exports = {
  siteMetadata: {
    title: `Seattle Tip Jar`,
    description: `Gatsby site for seattletipjar.com`,
    author: `@jfarris`,
    siteUrl: `https://seattletipjar.com`
  },
  plugins: [
    `gatsby-plugin-sitemap`,
    `gatsby-plugin-react-helmet`,
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `images`,
        path: `${__dirname}/src/images`,
      },
    },
    `gatsby-transformer-sharp`,
    `gatsby-plugin-sharp`,
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `gatsby-starter-default`,
        short_name: `starter`,
        start_url: `/`,
        background_color: `#663399`,
        theme_color: `#663399`,
        display: `minimal-ui`,
      },
    },
    "gatsby-plugin-sass",
    {
      resolve: `gatsby-plugin-google-analytics`,
      options: {
        trackingId: "UA-45497062-3",
        head: false,
        anonymize: true,
        respectDNT: true,
      },
    },
    {
      resolve: 'gatsby-plugin-robots-txt',
      options: {
        host: 'https://www.seattletipjar.com',
        sitemap: 'https://www.seattletipjar.com/sitemap.xml',
        policy: [{ userAgent: '*', allow: '/' }]
    }
  }
  ],
};
