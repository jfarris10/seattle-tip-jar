import React, { useState } from "react";

import { Link } from "gatsby";
import Layout from "../components/layout";
import SEO from "../components/seo";
import RandomButton from "../components/RandomButton";
import RandomPerson from "../components/RandomPerson";
import social1 from "../images/social-1.png";
import social2 from "../images/social-2.png";
import social3 from "../images/social-3.png";
import social4 from "../images/social-4.png";
import geekwire from "../images/geekwire.png";
import myballard from "../images/myballard.png";
import { Helmet } from "react-helmet"




class Application extends React.Component {
  render() {
    return (
      <div className="application">
        <Helmet>
          <meta charSet="utf-8" />
          <title>Seattle Tip Jar</title>
          <link rel="canonical" href="http://seattletipjar.com"/>
          <meta property="og:title" content="Tip Seattle Restaurant Staff Directly"/>
          <meta property="og:site_name" content="Seattle Tip Jar"/>
          <meta property="og:url" content="SeattleTipJar.com"/>
          <meta property="og:description" content="Drinking at home? Making your own food? Share some love forward by sending a tip to the staff at your favorite restaurant with Venmo, Paypal, and Cash App."/>
          <meta property="og:type" content="website"/>
          <meta property="og:image" content="https://zappy.zapier.com/d3d05e4092e6ab67e217acd8db4f1452.png"/>
          <meta name="twitter:card" content="summary"/>
          <meta name="twitter:site" content="@seattletipjar"/>
          <meta name="twitter:title" content="Tip Seattle Restaurant Staff Directly - SeattleTipJar.com"/>
          <meta name="twitter:description" content="Drinking at home? Making your own food? Share some love forward by sending a tip to the staff at your favorite restaurant with Venmo, Paypal, and Cash App."/>
          <meta name="twitter:image" content="https://seattletipjar.com/static/logo-3396d29ffb4f8642b3578a42d1214745.png"/>
          <link rel="canonical" href="http://seattletipjar.com/"/>
        </Helmet>
      </div>
    )
  }
}

const Intro = () => (
  <div className="intro">
    <h2>About Seattle Tip Jar</h2>
    <p>
      Seattle Tip Jar is an opportunity to share a small act of kindness with our
      Service Industry community during these uncertain times. Since Governor Inslee 
      issued a `stay-at-home` order on March 23rd, thousands of servers, bartenders, 
      baristas and other service industry workers have been laid off or furloughed.
    </p>
    <p>
      The premise is simple: Tip a dollar, $5, $10, $20 — or whatever you’d
      like — directly to a Service Industry worker! Tip someone at random or
      browse the <Link to="/donate">full list to find your favorite spots</Link>
      .
    </p>
    <p>
      If you’re a Service Industry worker,{" "}
      <Link to="/signup">fill out the form to receive tips</Link> directly
      through your Cashapp, Venmo, or PayPal.
    </p>
  </div>
);

const Pledge = () => (
  <div className="pledge">
    <h2>Take the pledge!</h2>
    <p>
      Take the pledge to tip every time you make a cocktail at home, miss going
      out, wish you were drinking a latte, wish you had a wood-fired oven, or
      don’t want to do the dishes.
    </p>
    
    <h3>Help spread the word and share on social media!</h3>
    <a href={social1}>
      <img src={social1} alt="Send some love on Seattle tip jar" />
    </a>
    <a href={social2}>
      <img
        src={social2}
        alt="I pledge to tip for every meal I wish someone else made"
      />
    </a>
    <a href={social3}>
      <img
        src={social3}
        alt="I pledge to tip for every drink I make at home because I really miss going out"
      />
    </a>
    <a href={social4}>
      <img
        src={social4}
        alt="I pledge to tip for every cup of coffee I wish was a latte"
      />
    </a>
    <div className="asSeenOn"><a href="https://www.geekwire.com/2020/hot-tip-friends-launch-seattle-tip-jar-help-service-industry-workers-covid-19/"><img src={geekwire} style={{height: 30}} alt="geekwirelogo"/></a> <a href="https://www.myballard.com/2020/04/10/seattle-tip-jar-launches-to-help-servers-bartenders-baristas-during-covid-19-closures/"><img src={myballard} style={{height: 30}} alt="myballard logo" /></a>
    </div></div>
);

const IndexPage = () => {
  const [randomPerson, setRandomPerson] = useState({});

  return (
    <Layout>
      <SEO title="Seattle Tip Jar" />
      <Intro />
      {Object.keys(randomPerson).length > 0 && (
        <RandomPerson
          person={randomPerson}
          clearPerson={() => setRandomPerson({})}
          random={true}
        />
      )}
      <RandomButton
        handleClick={setRandomPerson}
        text="Find a random person to tip"
      />
      <nav>
        <Link to="/donate">Browse the full list</Link>
        <Link to="/signup">Sign up to receive tips</Link>
      </nav>
      <Pledge />
    </Layout>
  );
};

export default IndexPage;
