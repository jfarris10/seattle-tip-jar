import React from "react";

import Layout from "../components/layout";
import SEO from "../components/seo";

const AboutPage = () => (
  <Layout>
    <SEO title="About" />
    <h1>About</h1>
    <p>
      Since the majority of citizens in the US are under `stay at home` orders, bars and restaurants have had to change drastically. Takeout and delivery are still allowed, but in most cases bars and restaurants have laid off or furloughed much of their staff. In Washington State this will be the case until 
      at least May 4th, and it will <a href="https://seattle.eater.com/2020/4/2/21204506/tom-douglas-restaurant-group-broke">take longer</a> for folks to get back on their feet. 
    </p>
    <p>
      This site was inspired by Jacqi and Noah's <a href="pdxtipjar.com">pdxtipjar.com</a>, a virtual "tip jar" for Portland's service industry workers. Simply put, if you've lost hours or your job, we hope you'll sign up to receive tips. If you've been fortunate enough to stay fully employed, we encourage you to tip as often as you can.
      </p>

    <p>Grateful for your support,</p>
    <p>Justin & Matt</p>
    <p>
      <a href="mailto:hey@seattletipjar.com">hey@seattletipjar.com</a>
    </p>
  </Layout>
);

export default AboutPage;
