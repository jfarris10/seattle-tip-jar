import React from "react";

const SignupForm = () => (
  <iframe
    title="Sign up"
    width="100%"
    height="800"
    style={{ border: "none" }}
    src="https://docs.google.com/forms/d/e/1FAIpQLScGpp2HT4D-w0axGDZXyDBMaEIeHXsOLpAVpY2EkV0MlWCBrw/viewform?usp=sf_link"
  />
);

export default SignupForm;
